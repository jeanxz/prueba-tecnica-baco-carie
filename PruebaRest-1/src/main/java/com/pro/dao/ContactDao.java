package com.pro.dao;

import org.springframework.data.repository.CrudRepository;

import com.pro.entity.Contact;

public interface ContactDao extends CrudRepository<Contact, Integer> {

}
